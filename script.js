/*****************************************************************************
 Son of Suckerfish Dropdowns
 http://htmldog.com/articles/suckerfish/dropdowns/
 
 With mouse out delay from the article here
 http://blog.peter-ryan.co.uk/2007/12/08/suckerfish-menus-with-hide-delay/
 *****************************************************************************/
sfHover = function() {
  var timeout = 600;
  var cssClass = "sfhover";

  var queue = [];
  var reCSS = new RegExp( "\\b" + cssClass + "\\b" );
  var sfEls = document.getElementById( "primary" ).getElementsByTagName( "li" );

  for( var i = 0; i < sfEls.length; i++ ) {
    // Mouseover and mouseout handlers for regular mouse based interface.
    sfEls[ i ].onmouseover = function() {
      queueFlush();
      this.className += " " + cssClass;
    }
    sfEls[ i ].onmouseout = function() {
      queue.push( [ setTimeout( queueTimeout, timeout ), this ] );
    }

    // Focus and blur handlers for keyboard based navigation.
    sfEls[ i ].onfocus = function() {
      queueFlush();
      this.className += " " + cssClass;
    }
    sfEls[ i ].onblur = function() {
      queue.push( [ setTimeout( queueTimeout, timeout ), this ] );
    }

    // Click event handler needed for tablet type interfaces (e.g. Apple iPhone).
    sfEls[ i ].onclick = function( e ) {
      if( this.className.search( reCSS ) == -1 ) {
        // CSS not set, so clear all sibling (and decendants) menus, and then set CSS on this menu...
        var elems = this.parentNode.getElementsByTagName( "li" );
        for( var i = 0; i < elems.length; i++ ) {
          elems[ i ].className = elems[ i ].className.replace( reCSS, "" );
        }
        this.className += " " + cssClass;
      } else {
        // CSS already set, so clear all decendant menus and then this menu...
        var elems = this.getElementsByTagName( "li" );
        for( var i = 0; i < elems.length; i++ ) {
          elems[ i ].className = elems[ i ].className.replace( reCSS, "" );
        }
        this.className = this.className.replace( reCSS, "" );
      }
      if( e && e.stopPropagation )
        e.stopPropagation();
      else
        window.event.cancelBubble = true;
    }
  }

  queueFlush = function () {
    while( queue.length ) {
      clearTimeout( queue[ 0 ][ 0 ] );
      queueTimeout();
    }
  }

  queueTimeout = function() {
    if( queue.length ) {
      var el = queue.shift()[ 1 ];
      el.className = el.className.replace( reCSS, "" );
    }
  }
}

function addLoadEvent( func ) {
  var oldonload = window.onload;
  if( typeof window.onload != 'function' ) {
    window.onload = func;
  } else {
    window.onload = function() {
      oldonload();
      func();
    }
  }
}

addLoadEvent( sfHover );
