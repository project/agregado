<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <!--[if lte IE 7]>
    <link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path ?><?php print path_to_theme() ?>/fix-ie.css" />
  <![endif]-->
  <?php print $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
</head>

<body>
  <div id="upper">
    <div class="container">
      <div id="primary-and-search-box">
        <?php if (!empty($primary_links)): ?>
          <div id="primary">
            <?php print menu_tree($menu_name = 'primary-links') ?>
          </div> <!-- /#primary -->
        <?php endif; ?>

        <?php if (!empty($search_box)): ?>
          <div id="search-box">
            <?php print $search_box; ?>
          </div> <!-- /#search-box -->
        <?php endif; ?>
      </div> <!-- /#nice-menu-and-search-box -->

      <div id="site-name-and-slogan" class="clear-block">
        <?php if (!empty($site_name)): ?>
          <div id="site-name">
            <a href="<?php print $front_page ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
          </div> <!-- /#site-name -->
        <?php endif; ?>

        <?php if (!empty($site_slogan)): ?>
          <div id="site-slogan">
            <?php print $site_slogan; ?>
          </div> <!-- /#site-slogan -->
        <?php endif; ?>
      </div> <!-- /#site-name-and-slogan -->

      <div id="main-and-sidebar" class="clear-block">
        <div id="main">
          <?php if (!empty($mission)): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
          <?php if (!empty($title)): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
          <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
          <?php if (!empty($messages)): ?><div class="clear-block"><?php print $messages; ?></div><?php endif; ?>
          <?php if (!empty($help)): ?><div class="clear-block"><?php print $help; ?></div><?php endif; ?>
          <div id="content" class="clear-block">
            <?php print $content; ?>
          </div> <!-- /#content -->
          <?php print $feed_icons; ?>
        </div> <!-- /#main -->

        <div id="sidebar">
          <?php print $sidebar; ?>
        </div> <!-- /#sidebar -->
      </div> <!-- /#main-and-sidebar -->

      <?php if (!empty($bottom)): ?>
        <div id="bottom-region" class="clear-block">
          <?php print $bottom; ?>
        </div> <!-- /#bottom-region -->
      <?php endif; ?>
    </div> <!-- /.container -->
  </div> <!-- /#upper -->

  <div id="lower" class="clear-block">
    <div id="bottom">
      <div class="container">
        <div id="bottom-left-and-centre">
          <div id="bottom-left">
            <?php print $bottom_left; ?>
          </div> <!--/#bottom-left -->

          <div id="bottom-centre">
            <?php print $bottom_centre; ?>
          </div> <!-- /#bottom-centre -->

        </div> <!-- /#bottom-left-and-centre -->

        <div id="bottom-right">
          <?php print $bottom_right; ?>
        </div> <!-- /#bottom-right -->
      </div> <!-- /.container -->
    </div> <!-- /#bottom -->

    <div id="footer-wrapper" class="clear-block">
      <div class="container">
        <?php if (!empty($secondary_links)): ?>
          <div id="secondary">
            <?php print theme('links', $secondary_links, array('class' => 'secondary-links')); ?>
          </div> <!-- /#secondary -->
        <?php endif; ?>

        <div id="footer">
          <?php print $footer_message; ?>
          <div id="theme-credits" class="clear-block"><?php print '<a href="http://drupal.org/project/agregado" title="Download Agregado Drupal theme.">Agregado</a> Drupal ' . t('theme by') . ' ' . '<a href="http://kahthong.com/" title="Visit Leow Kah Thong\'s website.">Leow Kah Thong</a><br />' . t('Designed by') . ' <a href="http://www.darrenhoyt.com/" title="Visit Darren Hoyt\'s website.">Darren Hoyt</a>'; ?></div>
        </div> <!-- /#footer -->
      </div> <!-- /.container -->
    </div> <!-- /#footer-wrapper -->
  </div> <!-- /#lower -->

  <?php print $closure; ?>
</body>
</html>
