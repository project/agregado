============
Installation
============

1.  Download Agregado from http://drupal.org/project/agregado

2.  Unpack the downloaded file, place the entire agregado folder in your Drupal
    installation under any one of the following directory:

      sites/all/themes
        making it available to the default Drupal site and to all Drupal sites
        in a multi-site configuration
      sites/default/themes
        making it available to only the default Drupal site
      sites/example.com/themes
        making it available to only the example.com site if there is a
        sites/example.com/settings.php configuration file

3.  Log in as administrator on your Drupal site and go to Administer > Site
    building > Themes (admin/build/themes) and make Agregado the default theme.

4.  For the dropdown menus to work (with Primary links), please make sure that
    the parent menu items' Expanded property is checked.

5.  You are done!

6.  If you have any further questions, theme customisation services, PSD to
    Drupal, just send me an e-mail with further details at ktleow@gmail.com

====================
Compatibility issues
====================
Agregado theme's Suckerfish Dropdown is known to have issues with DHTML Menus.
The dropdowns will not work with DHTML Menus enabled.
