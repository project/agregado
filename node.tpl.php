<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> clear-block">
  <?php print $picture ?>

  <?php if (!$page): ?>
    <?php if ($teaser && user_access('administer nodes')): ?>
      <div class="quick-edit-links">
        <ul class="links inline">
          <li class="first quick-edit"><?php print l('[Edit]', 'node/' . $nid . '/edit'); ?></li>
          <li class="last quick-delete"><?php print l('[Delete]', 'node/' . $nid . '/delete'); ?></li>
        </ul>
      </div>
    <?php endif; ?>

    <h2 class="title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php endif; ?>

  <?php if ($submitted): ?>
    <span class="submitted"><?php print $submitted ?> &#8211; </span>

    <?php if ($comment == 1 || $comment == 2) {
          if ($node->comment_count == 0) {
            print '<span class="comment-count">' . t('No comments') . '</span>';
          } elseif ($node->comment_count == 1) {
            print '<span class="comment-count">' . t('1 comment') . '</span>';
          } else {
            print '<span class="comment-count">' . $node->comment_count . ' ' . t('comments') . '</span>';
          }
      } else {
        print t('Comments disabled');
      }
    ?>
  <?php endif; ?>

  <?php if ($terms): ?>
    <div class="terms">
      <?php print t('Tags') . ':' . $terms; ?>
    </div>
  <?php endif;?>

  <div class="content">
    <?php print $content ?>
  </div>

  <div class="control-links">
    <?php print $links; ?>
  </div>
</div>
