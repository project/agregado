<div id="comment-<?php print $comment->cid ?>" class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ($comment->uid == $node->uid) ? ' comment-by-author-of-post' : ''; print ' '. $status ?> clear-block">
  <?php print $picture ?>

  <h3><?php print $title ?></h3>

  <div class="submitted"><?php print $submitted ?></div>

  <div class="content">
    <?php print $content ?>
    <?php if ($signature): ?>
      <div class="user-signature clear-block">
        <?php print $signature ?>
      </div>
    <?php endif; ?>
  </div>

  <div class="control-links">
    <?php print $links ?>
  </div>
</div>
